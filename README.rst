====
FAQ
=====



Quick start
-----------

1. Add "FAQ" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'faq',
    ]

2. Include the FAQ URLconf in your project urls.py like this::

    path('faq/', include('FAQ.urls')),

3. Run ``python manage.py migrate`` to create the FAQ models.

4. Add FAQGroup.add_user_to_group(user, type) to start adding user to group.

5. In section set which group can see section.

6. If you wanna to display FAQ in custom temple for target user, you must use:

    {% for section in sections %}
        {{ section }} #display section name
        {% for question in section.to_display %} #display question with tag display true
            {{ question.question }}
            {{ question.answer }}
        {% endfor %}
    {% endfor %}


For non login user you have group public. If you change name this group in database you must set it name in settings in DEFAULT_GROUP ='group_name'
