"""Admin classes."""

# Django
from django.contrib import admin

# Project
from faq.models import FAQGroup
from faq.models import Question
from faq.models import Section


@admin.register(Section)
class SectionAdmin(admin.ModelAdmin):  # noqa: D101
    pass


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):  # noqa: D101
    list_display = ['question', 'section']


@admin.register(FAQGroup)
class FAQGroupAdmin(admin.ModelAdmin):  # noqa: D101
    pass
