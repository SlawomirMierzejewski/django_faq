"""faq Models."""
# Django
from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import ugettext_lazy as _


class FAQGroup(models.Model):
    """faq Group Model."""

    group_name = models.CharField(max_length=255)
    user = models.ManyToManyField(get_user_model())

    class Meta:  # noqa: D106
        verbose_name = _('faq Group')
        verbose_name_plural = _('faq Groups')

    @classmethod
    def add_user_to_group(cls, user, group):
        """Add user after register to proper group. If group doesn't exist, it creates it."""
        obj = cls.objects.get_or_create(group_name=group)
        obj[0].user.add(user)
        obj[0].save()

    @classmethod
    def group_id(cls, user):
        """Return groups where user is assigned."""
        return cls.objects.filter(user=user)

    def __str__(self):  # noqa: D105
        return self.group_name


class Section(models.Model):
    """Section model."""

    name = models.CharField(_('Name'), max_length=200)
    user_group = models.ManyToManyField(FAQGroup)
    display = models.BooleanField(_('Display'))

    class Meta:  # noqa: D106
        verbose_name = _('Section')
        verbose_name_plural = _('Sections')

    def __str__(self):  # noqa: D105
        return self.name

    def to_display(self):
        """Return question, for current section, where display is set to True."""
        return Question.objects.filter(display=True, section=self)


class Question(models.Model):
    """Question model."""

    section = models.ForeignKey(
        Section,
        on_delete=models.CASCADE,
        verbose_name=_('Section'),
        related_name='question')
    question = models.TextField(_('Question'))
    answer = models.TextField(_('Answer'))
    display = models.BooleanField(_('Display'), default=False)

    class Meta:  # noqa: D106
        verbose_name = _('Question')
        verbose_name_plural = _('Questions')

    def __str__(self):  # noqa: D105
        return self.question
