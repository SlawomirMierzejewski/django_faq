"""faq views."""

# Django
from django.conf import settings
from django.contrib.auth.decorators import user_passes_test
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import CreateView
from django.views.generic import DeleteView
from django.views.generic import ListView
from django.views.generic import UpdateView

# Project
from faq import default_settings
from faq.decorator import admin_required
from faq.models import FAQGroup
from faq.models import Question
from faq.models import Section


class FAQListView(ListView):
    """Display all section and question for current user."""

    model = Section
    paginate_by = 25
    template_name = 'FAQ_list.html'

    def get_context_data(self, *, object_list=None, **kwargs):  # noqa: D102
        user = self.request.user
        if user.is_authenticated:
            group = FAQGroup.group_id(user)
        else:
            group = FAQGroup.objects.filter(
                group_name=getattr(settings, 'DEFAULT_GROUP', default_settings.DEFAULT_GROUP))
        sections = Section.objects.filter(display=True, user_group__in=group)
        context = {'sections': sections}
        return context


@method_decorator([admin_required], name='dispatch')
class ManageFAQListView(ListView):
    """Display List of section where we can manage theme."""

    model = Section
    paginate_by = 25
    template_name = 'manageFAQ.html'


@method_decorator(user_passes_test(lambda u: u.is_superuser), name='dispatch')
class QuestionUpdateView(UpdateView):
    """Question UpdateView."""

    model = Question
    fields = '__all__'
    template_name = 'question_update.html'

    def get_success_url(self):  # noqa: D102
        return reverse('manage_faq')


@method_decorator(user_passes_test(lambda u: u.is_superuser), name='dispatch')
class QuestionCreateView(CreateView):
    """Question CreateView."""

    model = Question
    fields = ['question', 'answer', 'display']
    template_name = 'question_add.html'

    def form_valid(self, form):  # noqa: D102
        question = form.cleaned_data['question']
        answer = form.cleaned_data['answer']
        display = form.cleaned_data['display']
        section = Section.objects.get(pk=self.kwargs.get('section_pk'))
        new_question = Question.objects.create(
            question=question,
            answer=answer,
            display=display,
            section=section)
        new_question.save()
        return HttpResponseRedirect(reverse('manage_faq'))

    def get_success_url(self):  # noqa: D102
        return reverse('faq')


@method_decorator(user_passes_test(lambda u: u.is_superuser), name='dispatch')
class SectionCreateView(CreateView):
    """CreateView for section model."""

    model = Section
    fields = '__all__'
    template_name = 'section_add.html'

    def get_success_url(self):  # noqa: D102
        return reverse('manage_faq')


@method_decorator(user_passes_test(lambda u: u.is_superuser), name='dispatch')
class SectionUpdateView(UpdateView):
    """UpdateView for section object."""

    model = Section
    fields = '__all__'
    template_name = 'section_update.html'

    def get_success_url(self):  # noqa: D102
        return reverse('manage_faq')


@method_decorator(user_passes_test(lambda u: u.is_superuser), name='dispatch')
class QuestionDeleteView(DeleteView):
    """DeleteView for object from question model."""

    model = Question
    template_name = 'delete.html'

    def get_success_url(self):  # noqa: D102
        return reverse('manage_faq')


@method_decorator(user_passes_test(lambda u: u.is_superuser), name='dispatch')
class SectionDeleteView(DeleteView):
    """DeleteView for object from section model."""

    model = Section
    template_name = 'delete.html'

    def get_success_url(self):  # noqa: D102
        return reverse('manage_faq')


@method_decorator(user_passes_test(lambda u: u.is_superuser), name='dispatch')
class FAQGroupListView(ListView):
    """ListView for FAQGroup model."""

    model = FAQGroup
    paginate_by = 25
    template_name = 'FAQGroup_list.html'


@method_decorator(user_passes_test(lambda u: u.is_superuser), name='dispatch')
class FAQGroupCreateView(CreateView):
    """Create View for FAQGroup model."""

    model = FAQGroup
    fields = '__all__'
    template_name = 'FAQGroup_add.html'

    def get_success_url(self):  # noqa: D102
        return reverse('list_faq_group')


@method_decorator(user_passes_test(lambda u: u.is_superuser), name='dispatch')
class FAQGroupUpdateView(UpdateView):
    """UpdateView fro FAQGroup model."""

    model = FAQGroup
    fields = '__all__'
    template_name = 'FAQGroup_update.html'

    def get_success_url(self):  # noqa: D102
        return reverse('list_faq_group')


@method_decorator(user_passes_test(lambda u: u.is_superuser), name='dispatch')
class FAQGroupDeleteView(DeleteView):
    """Delete View for FAQGroup model."""

    model = FAQGroup
    template_name = 'delete.html'

    def get_success_url(self):  # noqa: D102
        return reverse('list_faq_group')
