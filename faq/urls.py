"""faq URL Configuration."""
# Django
from django.urls import path

# Project
from faq.views import FAQGroupCreateView
from faq.views import FAQGroupDeleteView
from faq.views import FAQGroupListView
from faq.views import FAQGroupUpdateView
from faq.views import FAQListView
from faq.views import ManageFAQListView
from faq.views import QuestionCreateView
from faq.views import QuestionDeleteView
from faq.views import QuestionUpdateView
from faq.views import SectionCreateView
from faq.views import SectionDeleteView
from faq.views import SectionUpdateView

urlpatterns = [
    path('', FAQListView.as_view(), name='faq'),
    path('manage/', ManageFAQListView.as_view(), name='manage_faq'),
    path('add-question/<int:section_pk>', QuestionCreateView.as_view(), name='add_question'),
    path('update-question/<int:pk>', QuestionUpdateView.as_view(), name='update_question'),
    path('delete-question/<int:pk>', QuestionDeleteView.as_view(), name='delete_question'),
    path('add-section/', SectionCreateView.as_view(), name='add_section'),
    path('update-section/<int:pk>', SectionUpdateView.as_view(), name='update_section'),
    path('delete-section/<int:pk>', SectionDeleteView.as_view(), name='delete_section'),
    path('add-faq-group/', FAQGroupCreateView.as_view(), name='add_faq_group'),
    path('list-faq-group/', FAQGroupListView.as_view(), name='list_faq_group'),
    path('update-faq-group/<int:pk>', FAQGroupUpdateView.as_view(), name='update_faq_group'),
    path('delete-faq-group/<int:pk>', FAQGroupDeleteView.as_view(), name='delete_faq_group'),

]
