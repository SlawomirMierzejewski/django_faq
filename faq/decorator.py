"""Decorators."""
# Django
from django.contrib.auth.decorators import user_passes_test


def admin_required(function=None, redirect_field_name=None):
    """Admin required decorator."""
    actual_decorator = user_passes_test(
        lambda u: u.is_active and u.is_staff and u.is_superuser,
        redirect_field_name=redirect_field_name,
    )
    if function:
        return actual_decorator(function)
    return actual_decorator
